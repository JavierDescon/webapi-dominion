﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebApiDominion.Models
{
    public class BdConection
    {
        static SqlConnection sqlConnection;
        public static SqlConnection conectar() {
            string server = ConfigurationManager.AppSettings["ServidorSQL"];
            string user = ConfigurationManager.AppSettings["UsuarioSQL"];
            string bd = ConfigurationManager.AppSettings["database"];
            string pass = ConfigurationManager.AppSettings["PasswordSQL"];

            sqlConnection = new SqlConnection("server='" + server + "';database='" + bd + "';uid='" + user + "';password='" + pass + "'");
            sqlConnection.Open();
            return sqlConnection;
        }
    }
}