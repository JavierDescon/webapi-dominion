﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiDominion.Models
{
    public class Precios_Stock
    {
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string TotalStock { get; set; }
        public string Onhand { get; set; }
        public string WhsCode { get; set; }
        public string PrecioTotal { get; set; }
    }

   
}