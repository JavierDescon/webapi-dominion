﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiDominion.Models
{
    public static class Querys
    {
        
        public static string QueryStock(string item_code,string Card_code,string almacen) {
            string query= "SELECT T0.ItemCode,t0.Itemname,t0.onhand as [Total stock],T1.ONHAND,t1.WhsCode , " +
                "((select T2.Price from ITM1 T2  where T2.ItemCode=t0.ItemCode and  T2.PriceList=" +
                "(select ListNum from OCRD  where CardCode='" + Card_code + "')) - " +
                "ISNULL((select T2.Price from ITM1 T2 where T2.ItemCode=t0.ItemCode and T2.PriceList=" +
                "(select ListNum from OCRD where CardCode='" + Card_code + "')) * " +
                "((Select Discount from OSPP where ItemCode= '" + item_code + "' and CardCode='" + Card_code + "')/100),0)) + " +
                "iif(T0.IndirctTax='Y',U_DMARendimiento,0) + " +
                "ISNULL(CASE WHEN T0.U_IVA='0.08' THEN 0  WHEN T0.U_IVA='16.00' AND T0.TaxCodeAR='IV/EPBS' THEN " +
                "(((select T2.Price from ITM1 T2  where T2.ItemCode=t0.ItemCode and  T2.PriceList=" +
                "(select ListNum from OCRD  where CardCode='" + Card_code + "')) - " +
                "ISNULL((select T2.Price from ITM1 T2 where T2.ItemCode=t0.ItemCode and T2.PriceList=" +
                "(select ListNum from OCRD where CardCode='" + Card_code + "')) * " +
                "((Select Discount from OSPP where ItemCode= '" + item_code + "' and CardCode='" + Card_code + "')/100),0))  + U_DMARendimiento) * 0.16 " +
                "WHEN T0.U_IVA='16.00' THEN (((select T2.Price from ITM1 T2  where T2.ItemCode=t0.ItemCode and  T2.PriceList=" +
                "(select ListNum from OCRD  where CardCode='" + Card_code + "')) - " +
                "ISNULL((select T2.Price from ITM1 T2 where T2.ItemCode=t0.ItemCode and T2.PriceList=" +
                "(select ListNum from OCRD where CardCode='" + Card_code + "')) * " +
                "((Select Discount from OSPP where ItemCode= '" + item_code + "' and CardCode='" + Card_code + "')/100),0))) * 0.16 END ,0)" +
                " + IIF(T0.U_IEPS='8.00',(((select T2.Price from ITM1 T2  where T2.ItemCode=t0.ItemCode and  T2.PriceList=" +
                "(select ListNum from OCRD  where CardCode='" + Card_code + "')) - " +
                "ISNULL((select T2.Price from ITM1 T2 where T2.ItemCode=t0.ItemCode and T2.PriceList=" +
                "(select ListNum from OCRD where CardCode='" + Card_code + "')) * " +
                "((Select Discount from OSPP where ItemCode='" + item_code + "' and CardCode='" + Card_code + "')/100),0)))*0.08,0)  AS 'PrecioTotal' " +
                " from OITM T0 INNER JOIN OITW T1 ON T0.ItemCode = T1.ItemCode  where T1.itemCode = '" + item_code + "' and T1.WhsCode = '" + almacen + "'"; 
            return query;
        }
    }
}