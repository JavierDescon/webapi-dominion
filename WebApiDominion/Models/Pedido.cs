﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiDominion.Models
{
    public class Pedido
    {
        public string docEntry { get; set; }
        public string CardCode { get; set; }
        public string DocDate { get; set; }
        public string CardName { get; set; }
        public string DocTotal { get; set; }
        public string IVA { get; set; }
        public string TipoDireccion { get; set; }
        public string Comments { get; set; }

    }
    public class PedidoPerzonaizado
    {
        public string cardCode { get; set; }
        public string docDate { get; set; }
        public string docEntry { get; set; }
        public string docNum { get; set; }
        public string cardName { get; set; }
        public string docTotal { get; set; }
        public List<Lineas> lineas;
    }

    public class verPedido
    {
        public Pedido pedido;
        public List<Lineas> Lineas;
    }

    public class creaPedido
    {
        public string CardCode { get; set; }
        public string DocDate { get; set; }
        public string DocDueDate { get; set; }
        public string DocEntry { get; set; }
        public string DocNum { get; set; }
        public string SlpCode { get; set; }
        public string CardName { get; set; }
        public string DocTotal { get; set; }
        public string TipoDireccion { get; set; }
        public List<Lineas> lineas;
    }
}