﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiDominion.Models
{
    public class ListaPrecios
    {
        public string noPriceList;
        public string itemCode;
        public string price;
        public string currency;
        public string factor;
    }
}