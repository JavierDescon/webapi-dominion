﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiDominion.Models;

namespace WebApiDominion.Controllers
{
    [Authorize]
    public class StockController : ApiController
    {
        

        // GET: api/Stock/5
        public List<Precios_Stock> Get(string ItemCode, string CardCode,string Almacen)
        {
            List<Precios_Stock> listaStock = new List<Precios_Stock>();
            SqlCommand query = new SqlCommand("Execute ItemsInStock '"+CardCode+"','"+ItemCode+"','"+Almacen+"'", BdConection.conectar());
            SqlDataReader reader = query.ExecuteReader();
            while (reader.Read())
            {
                Precios_Stock stockTemp = new Precios_Stock();
                stockTemp.ItemCode = reader["ItemCode"].ToString();
                stockTemp.ItemName = reader["ItemName"].ToString();
                stockTemp.TotalStock = reader["Total stock"].ToString();
                stockTemp.Onhand = reader["ONHAND"].ToString();
                stockTemp.WhsCode = reader["WhsCode"].ToString();
                stockTemp.PrecioTotal = reader["PrecioTotal"].ToString();
                listaStock.Add(stockTemp);
            }
            return listaStock;
        }

    
    }
}
