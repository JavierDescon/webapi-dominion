﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiDominion.Models;
using SAPbobsCOM;


namespace WebApiDominion.Controllers
{
    [Authorize]
    public class CotizacionController : ApiController
    {
        string server;
        string user;
        string bd;
        string pass;

        public string sErrMsg;
        public int lErrCode;
        public int lRetCode;
        public Boolean Conectado = false;
        public Company oCompany;

        public CotizacionController()
        {
            server = ConfigurationManager.AppSettings["ServidorSQL"];
            user = ConfigurationManager.AppSettings["UsuarioSQL"];
            bd = ConfigurationManager.AppSettings["database"];
            pass = ConfigurationManager.AppSettings["PasswordSQL"];

        }

        private Resultado ConectaSAP(int tipo)
        {
            Resultado response = new Resultado();
            try
            {
                server = ConfigurationSettings.AppSettings["ServidorSQL"];
                user = ConfigurationSettings.AppSettings["UsuarioSQL"];
                bd = ConfigurationSettings.AppSettings["database"];
                pass = ConfigurationSettings.AppSettings["PasswordSQL"];
                string LicenseServer = ConfigurationSettings.AppSettings["LicenseServer"];
                string DbUserName = ConfigurationSettings.AppSettings["usuario"];
                string DbPassword = ConfigurationSettings.AppSettings["pass"];
                string TipoSQL = ConfigurationSettings.AppSettings["TipoSQL"];


                oCompany = new Company();

                oCompany.Server = server;
                oCompany.LicenseServer = LicenseServer;
                oCompany.DbUserName = user;
                oCompany.DbPassword = pass;

                if (TipoSQL == "2008")
                {
                    oCompany.DbServerType = (SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008);
                }
                else if (TipoSQL == "2012")
                {
                    oCompany.DbServerType = (SAPbobsCOM.BoDataServerTypes.dst_MSSQL2012);
                }
                else if (TipoSQL == "2014")
                {
                    oCompany.DbServerType = (SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014);
                }
                else if (TipoSQL == "2016")
                {
                    oCompany.DbServerType = (SAPbobsCOM.BoDataServerTypes.dst_MSSQL2016);
                }

                oCompany.UseTrusted = false;
                oCompany.CompanyDB = bd;
                oCompany.UserName = DbUserName;
                oCompany.Password = DbPassword;

                // Connecting to a company DB
                lRetCode = oCompany.Connect();

                if (lRetCode != 0)
                {
                    int temp_int = lErrCode;
                    string temp_string = sErrMsg;
                    oCompany.GetLastError(out temp_int, out temp_string);
                    response.error = true;
                    response.message = sErrMsg;
                    //MessageBox.Show(globals_Renamed.sErrMsg);
                }
                else
                {

                    response.error = false;
                    response.message = "Conectado";
                    // Disable controls

                }

            }
            catch (Exception ex)
            {
                response.error = true;
                response.message = ex.Message;
            }
            return response;

        }
            
        [HttpPost]
        
        public Resultado PostCotizacion(CreaCotizacion json)
        {

            Resultado response = new Resultado();

            string CardCode = json.CardCode;
            string DocDate = json.DocDate;
            string SlpCode = json.SlpCode;
            string ShipToCode = json.TipoDireccion;
            string OC = json.OC;
            var respuesta = ConectaSAP(1);
            if (respuesta.error == false)
            {
                Documents OCotizacion;
                OCotizacion = (Documents)oCompany.GetBusinessObject(BoObjectTypes.oQuotations);

                OCotizacion.CardCode = CardCode;
                OCotizacion.DocDate = Convert.ToDateTime(DocDate);
                OCotizacion.SalesPersonCode = Convert.ToInt32(SlpCode);
                OCotizacion.ShipToCode = ShipToCode;
                OCotizacion.NumAtCard = OC; // "Cotización creada desde el Portal";

                //Aqui creamos las lineas
                foreach (var linea in json.lineas)
                {
                    string ItemCode = linea.ItemCode;
                    double quantity = linea.quantity;
                    double price = linea.price;
                    OCotizacion.Lines.TaxCode ="A24";
                    OCotizacion.Lines.ItemCode = ItemCode;
                    OCotizacion.Lines.Quantity = quantity;
                    OCotizacion.Lines.Price = price;
                    OCotizacion.Lines.Add();
                }
                //Agregar Documento 
                lErrCode = OCotizacion.Add();

                if (lErrCode != 0)
                {
                    int temp_int = lErrCode;
                    string temp_string = sErrMsg;
                    oCompany.GetLastError(out temp_int, out temp_string);
                    response.error = true;
                    response.message = temp_string;


                    //Poner mensaje de error que lea esto
                    //resultado = temp_string;
                    //MessageBox.Show(temp_string);
                }
                else
                {
                    //Aqui cuando se crea la APS correctamente
                    // = "APS was added successfully";

                    oCompany.Disconnect();
                    response.error = false;
                    response.message = "Cotización generada con exíto";
                }
            }
            else
            {
                response.error = true;
                response.message = respuesta.message;
            }

            return response;
        }
    }
}
