﻿using SAPbobsCOM;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiDominion.Models;

namespace WebApiDominion.Controllers
{
    [Authorize]
    public class PedidoController : ApiController
    {
        string server;
        string user;
        string bd;
        string pass;
        public string sErrMsg;
        public int lErrCode;
        public int lRetCode;
        public bool conectado = false;
        public Company oCompany;

        List<PedidoPerzonaizado> listaPedidos;

        public PedidoController() {
            server = ConfigurationManager.AppSettings["ServidorSQL"];
            user = ConfigurationManager.AppSettings["UsuarioSQL"];
            bd = ConfigurationManager.AppSettings["database"];
            pass = ConfigurationManager.AppSettings["PasswordSQL"];
        }

        public string GetEstado(int docEntry) {
            string estatus="Estado: ";
            SqlCommand query= new SqlCommand("SELECT DocStatus FROM ORDR WHERE docEntry ="+docEntry,BdConection.conectar());
            SqlDataReader read = query.ExecuteReader();
            while (read.Read())
            {
                estatus = read["DocStatus"].ToString();
            }
            if (estatus == "O")
            {
                estatus = "Estado: Abierto";
            }
            else if (estatus == "C")
            {
                estatus = "Estado: Cerrado";
            }
            else {
                estatus += "Estado: no se pudo obtener el estatus";
            }

            return estatus;
        }
               
        [HttpPost]
        public Resultado PostPedido(creaPedido pedido) {

            Resultado response = new Resultado();
            int DocEntry = Convert.ToInt32(pedido.DocEntry);
            string CardCode = pedido.CardCode;
            string DocDate = pedido.DocDate;
            string DocDueDate = pedido.DocDueDate;
            string SlpCode = pedido.SlpCode;
            string ShipToCode = pedido.TipoDireccion;
            string comentarios = "Basado en oferta de ventas " + DocEntry;

            if (conectado==false)
            {
                ConectaSAP();
            }
            Documents OPedido;
            OPedido = (Documents)oCompany.GetBusinessObject(BoObjectTypes.oOrders);
            
            OPedido.CardCode = CardCode;
            
            OPedido.DocDate = Convert.ToDateTime(DocDate);
            OPedido.DocDueDate = Convert.ToDateTime(DocDueDate);
            OPedido.SalesPersonCode = Convert.ToInt32(SlpCode);
            OPedido.Comments = comentarios;
            OPedido.ShipToCode = ShipToCode;
            //OPedido.NumAtCard = "Cotización creada desde el Portal";

            foreach (var linea in pedido.lineas)
            {
                //OPedido.Lines.TaxCode = "B16";
                OPedido.Lines.ItemCode = linea.ItemCode;
                OPedido.Lines.Quantity = linea.quantity;
                //OPedido.Lines.Price = linea.price;

                OPedido.Lines.BaseType = 23;
                OPedido.Lines.BaseLine = linea.LineNum;
                OPedido.Lines.BaseEntry = DocEntry;

                OPedido.Lines.Add();
            }
            lErrCode = OPedido.Add();
            if (lErrCode!=0)
            {
                int temp_int= lErrCode;
                string temp_string = sErrMsg;
                oCompany.GetLastError(out temp_int, out temp_string);
                response.error = true;
                response.message = temp_string;
            }
            else
            {
                oCompany.Disconnect();
                response.error = false;
                response.message = "Pedido Generado con extito";
            }

            return response;
            
        }

        private void ConectaSAP()
        {
            try
            {
                server = ConfigurationSettings.AppSettings["ServidorSQL"];
                user = ConfigurationSettings.AppSettings["UsuarioSQL"];
                bd = ConfigurationSettings.AppSettings["database"];
                pass = ConfigurationSettings.AppSettings["PasswordSQL"];
                string LicenseServer = ConfigurationSettings.AppSettings["LicenseServer"];
                string DbUserName = ConfigurationSettings.AppSettings["usuario"];
                string DbPassword = ConfigurationSettings.AppSettings["pass"];
                string TipoSQL = ConfigurationSettings.AppSettings["TipoSQL"];


                oCompany = new Company();

                oCompany.Server = server;
                oCompany.LicenseServer = LicenseServer;
                oCompany.DbUserName = user;
                oCompany.DbPassword = pass;

                if (TipoSQL == "2008")
                {
                    oCompany.DbServerType = (BoDataServerTypes.dst_MSSQL2008);
                }
                else if (TipoSQL == "2012")
                {
                    oCompany.DbServerType = (BoDataServerTypes.dst_MSSQL2012);
                }
                else if (TipoSQL == "2014")
                {
                    oCompany.DbServerType = (BoDataServerTypes.dst_MSSQL2014);
                }
                else if (TipoSQL == "2016")
                {
                    oCompany.DbServerType = (BoDataServerTypes.dst_MSSQL2016);
                }

                oCompany.UseTrusted = false;
                oCompany.CompanyDB = bd;
                oCompany.UserName = DbUserName;
                oCompany.Password = DbPassword;

                // Connecting to a company DB
                lRetCode = oCompany.Connect();

                if (lRetCode != 0)
                {
                    int temp_int = lErrCode;
                    string temp_string = sErrMsg;
                    oCompany.GetLastError(out temp_int, out temp_string);
                    //MessageBox.Show(globals_Renamed.sErrMsg);
                }
                else
                {

                    conectado = true;
                    // Disable controls

                }

            }
            catch (Exception ex)
            {

            }

        }
    }
}
